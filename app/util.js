'use strict'

const base58 = require('bs58');
const {sha256} = require('js-sha256');
const sRand = require('secure-random');

const PRIVATE_KEY_MAX = Buffer.from('0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364140', 'hex');

const generatePrivateKeyWIF = (privateKey) => {
  const prefixedPrivateKey = Buffer.from('80' + privateKey, 'hex');
  const hash = sha256(Buffer.from(sha256(prefixedPrivateKey), 'hex'));
  const checkSum = hash.substring(0, 8);
  return base58.encode(Buffer.from(privateKey.toString('hex') + checkSum), 'hex');
}

const generatePublicAddress = (publicKeyHash) => {
  const prefixedHash = Buffer.from('00' + publicKeyHash.toString('hex'), 'hex');
  const checkSum = sha256(Buffer.from(sha256(prefixedHash), 'hex')).substring(0, 8);
  const publicAddress = prefixedHash.toString('hex') + checkSum;
  return base58.encode(Buffer.from(publicAddress, 'hex'));
}

const generateValidKey = (retries) => {
  // Attempts <retries> times to generate a valid public key (below constant PRIVATE_KEY_MAX)
  if (retries === 0) {
    throw Error('Unable to successfully generate valid key');
  }
  const candidateKey = sRand.randomBuffer(32);
  if (Buffer.compare(PRIVATE_KEY_MAX, candidateKey) !== 1) {
    return candidateKey;
  }
  generateValidKey(retries--);
}

module.exports = {
  generatePrivateKeyWIF,
  generatePublicAddress,
  generateValidKey
}