`use strict`

const elliptic = require('elliptic').ec('secp256k1');
const ripemd160 = require('ripemd160');
const {sha256} = require('js-sha256');
const util = require('./util');

// Generate valid private key
const privateKey = util.generateValidKey(1000);

// Derive corresponding public key
const ellipticKeys = elliptic.keyFromPrivate(privateKey);

// Generate public key hash (sha256 + RIPEMD-160)
const hash = sha256(Buffer.from(ellipticKeys.getPublic('hex'), 'hex'));
const publicKeyHash = new ripemd160().update(Buffer.from(hash, 'hex')).digest();

// Generate public address
const publicAddress = util.generatePublicAddress(publicKeyHash);

// Generate private key WIF
const wif = util.generatePrivateKeyWIF(privateKey);

// print wallet details as JSON
console.log(JSON.stringify({
  address: publicAddress.toString('hex'),
  privateKey: privateKey.toString('hex'),
  publicKey: publicKeyHash.toString('hex'),
  wif: wif.toString('hex')
}, null, 4));
